from django.test import TestCase


class CoreTestCase(TestCase):

    def test_index_should_return_ok(self):
        response = self.client.get('/')

        self.assertEqual(response.status_code, 200)
